import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'

import './App.css';
import AddPerson from './components/AddPerson'
import PostList from './components/PostList'
import HomePage from './components/HomePage'
import DeletePerson from './components/DeletePerson'
import ModifyPerson from './components/ModifyPerson'
import SearchPerson from './components/SearchPerson'
import Login from './components/Login'
import Register from './components/Register'
import WelcomePage from './components/WelcomePage'






function App() {
  return (
      <Router>
        
        <div className="App">
          <Route path = "/" component = {HomePage} exact/>
          <Route path = "/addPerson" component = {AddPerson} />
          <Route path = "/deletePerson" component = {DeletePerson} />
          <Route path = "/modifyPerson" component = {ModifyPerson} />
          <Route path = "/searchPerson" component = {SearchPerson} />
          <Route path = "/postlist" component = {PostList} />
          <Route path = "/login" component = {Login} />
          <Route path = "/register" component = {Register} />
          <Route path = "/welcomepage" component = {WelcomePage} />

        </div>
      </Router>
      
    
  );
}

export default App;
