import React, { Component } from 'react';
import axios from 'axios'
import { Link } from 'react-router-dom'
import {Conf} from './Conf'
import logo from "../uvalogo.png"
import Button from '@material-ui/core/Button';
import Toolbar from "@material-ui/core/Toolbar";

class AddPerson extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstname : "",
            lastname : "",
            // birthdayDateTime : "",
            year : "",
            month : "",
            date : ""
        }
        this.state2 = {
            fullName : "",
            birthdayDateTime : ""
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount(){
        
        axios
			.get('http://'+Conf+':8080/birthdaybook/all')
			.then(response => {
                console.log("success")
                console.log(response.data)
                this.setState({
                    posts : response.data
                })
            })
			.catch(error => {
                console.log("fail")
				console.log(error)
            })
        
        
    }
    handleChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        })
    }

    
    handleSubmit(event){
        event.preventDefault()
        if (checkinvalid(this.state)) {
            return;
        }
        console.log(this.state)
        this.state2.fullName = this.state.firstname + ' ' + this.state.lastname
        this.state2.birthdayDateTime = this.state.year + "-" + this.state.month + "-" + this.state.date
        axios
			.post('http://'+Conf+':8080/birthdaybook', this.state2)
			.then(response => {
                alert("successfully add the person to the birthday book!")
				console.log(response)
			})
			.catch(error => {
				console.log(error)
			})
    }

    
    render() {
        const {firstname, lastname, year, month, date} = this.state
        return (
            <div>
                <Toolbar>
                    <Button variant="contained" color="secondary">
                        <Link to = "/postlist" style={{ textDecoration: 'none', color: 'white'}}> HOME </Link>
                        {/*HOME*/}
                    </Button>
                </Toolbar>
                <img src={logo} width={300} height={120} mode='fit' alt="Logo" />
                <div>
                    <h1>Please Add a Person ! </h1>
                    <br></br>

                </div>
                <form onSubmit = {this.handleSubmit}>
                    <div>
                        <label> First Name (e.g., John) </label>
                        <input type = 'text' 
                            name = "firstname"
                            value = {firstname}
                            onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <label> Last Name (e.g., Black) </label>
                        <input type = 'text'
                               name = "lastname"
                               value = {lastname}
                               onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <label> Year (e.g., 1999) </label>
                        <input type = 'text' 
                            name = "year"
                            value = {year}
                            onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <label> Month (e.g., 08) </label>
                        <input type = 'text'
                               name = "month"
                               value = {month}
                               onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <label> Date (e.g., 23) </label>
                        <input type = 'text'
                               name = "date"
                               value = {date}
                               onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        {/* <button type = 'submit'>Add Person</button> */}
                        <Button type = 'submit' variant="contained" color="primary">
                            ADD PERSON
                        </Button>
                    </div>
                    <br></br>
                    <div>
                        {/*Back To the <Link to ="/postList"> Main </Link> Page.*/}
                    </div>
                </form>
            </div>
        );
    }
}

export default AddPerson;

function checkinvalid(datatuple) {
    if (datatuple.firstname === '' || datatuple.lastname === '' || datatuple.year === '' || datatuple.month === ''|| datatuple.date === '') {
        alert('Invalid value: empty field');
        return true;
    }
    
    if (isNaN(datatuple.year) || parseInt(datatuple.year) >= 2020) {
        alert('Invalid value: invalid year');
        return true;
    }
    
    if (isNaN(datatuple.month) || parseInt(datatuple.month) > 12 || parseInt(datatuple.month) < 1) {
        alert('Invalid value: invalid month');
        return true;
    }
    
    if (isNaN(datatuple.date) || parseInt(datatuple.date) > 31 || parseInt(datatuple.date) < 1) {
        alert('Invalid value: invalid date');
        return true;
    }
    
    return false;
}
