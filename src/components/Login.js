import React, { Component } from 'react';
import axios from 'axios'
import logo from "../uvalogo.png"
import {Conf} from './Conf'
import Button from '@material-ui/core/Button';
import {Link} from "react-router-dom";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
           username : "",
           password : ""
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        })
    }

    
    handleSubmit(event){
        event.preventDefault()
        console.log(this.state)
        axios
			.post('http://'+Conf+':8080/birthdaybookuser/login', this.state)
			.then(response => {
                if(response.data === ""){
                    alert("Sorry you are not the user of the birthday book!")
                }else{
                    alert("Successfully Login!");
                    this.props.history.push("/welcomepage");
     
                }
                
				console.log(response)
			})
			.catch(error => {
				console.log(error)
			})
    }
   
    
    render() {
        return (
            <div>
                <img src={logo} width={300} height={120} mode='fit' alt="Logo" />
                <div>
                    <br></br>
                    <h1>Please Login here ! </h1>
                </div>
                <form onSubmit = {this.handleSubmit}>
                   
                    <div>
                        <label> Username </label>
                        <input type = 'text' 
                            name = "username"
                            value = {this.state.username} 
                            onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <label> Password </label>
                        <input type = 'password' 
                            name = "password"
                            value = {this.state.password} 
                            onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        {/*<button type = 'submit' color="secondary">Login!</button>*/}
                        <Button type = 'submit' variant="contained" color="primary">
                            Login
                        </Button>
                        <br></br>
                        <br></br>
                        <Link to = "/register" >Here to register!</Link>
                    </div>
                    <br></br>
                    
                </form>
            </div>
        );
            
    }
}

export default Login;