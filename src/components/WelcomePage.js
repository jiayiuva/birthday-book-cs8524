import React, { Component } from 'react';
import logo from "../uvalogo.png"
import { Link } from 'react-router-dom'
import Button from "@material-ui/core/Button";

class WelcomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };

    }

    render(){
        return(
        <div>
            <img src={logo} width={300} height={120} mode='fit' alt="Logo" />
            <div>
                <h1> Welcome back to your birthdaybook!</h1>
            </div>

            <div>
                <p style={{fontSize: '18px'}}>You can see who are in the Birthday Book now. Click here:</p>
                <Button variant="contained" color="primary">
                    <Link to = "/postlist" style={{ textDecoration: 'none', color: 'white'}}> See Birthday Book </Link>
                    {/*See Birthday Book*/}
                </Button>
                {/*<Link to = "/postlist" >You can see who are in your Birthday Book now.</Link>*/}
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                {/*Back to <Link to = "/" >Register/Login</Link>.*/}
                <Button variant="contained" color="default">
                    <Link to = "/" style={{ textDecoration: 'none', color: 'white'}}> Back to Register/Login </Link>
                </Button>
            </div>
        </div>
        );
    }
}

export default WelcomePage;
