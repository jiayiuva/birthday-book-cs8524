import React, { Component } from 'react';
import axios from 'axios'
import { Link } from 'react-router-dom'
// import Link from '@material-ui/core/Link';
import {Conf} from './Conf';
import logo from "../uvalogo.png"
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import Box from '@material-ui/core/Box';

class PostList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts :[]
        }
    }

    componentDidMount(){
        
        axios
			.get('http://'+Conf+':8080/birthdaybook/all')
			.then(response => {
                console.log("success")
                console.log(response.data)
                this.setState({
                    posts : response.data
                })
            })
			.catch(error => {
                console.log("fail")
				console.log(error)
            })
        
        
    }
    
    render() {
        const {posts} = this.state
        return (
            <div>
                <Toolbar>
                    <Button variant="contained" color="secondary">
                        <Link to = "/postlist" style={{ textDecoration: 'none', color: 'white'}}> HOME </Link>
                    </Button>
                    <Box m="0.5rem" />
                    <Button variant="contained" color="primary" >
                        <Link to = "/login" style={{ textDecoration: 'none', color: 'white'}}> Back to Register/Login </Link>
                    </Button>
                </Toolbar>

                <img src={logo} width={300} height={120} mode='fit' alt="Logo" />
                <h1>List of Person in Birthday Book</h1>
                <Button><Link to ="/addPerson" style={{ textDecoration: 'none', color: '#FF5A5F', fontSize: '18px'}}> ADD PERSON </Link></Button> |
                <Button><Link to ="/deletePerson" style={{ textDecoration: 'none', color: '#00A699', fontSize: '18px'}}> Delete PERSON </Link></Button> |
                <Button><Link to ="/modifyPerson" style={{ textDecoration: 'none', color: '#FF5A5F', fontSize: '18px'}}> Modify PERSON </Link></Button> |
                <Button><Link to ="/searchPerson"  style={{ textDecoration: 'none', color: '#00A699', fontSize: '18px'}}> Search PERSON </Link></Button>
                <br></br>
                {
                    posts.length ? 
                    posts.map(post => <div key = {post.id}> 
                        <div>
                            ID : {post.id}
                        </div>
                        <div>
                            Name : {post.fullName} 
                        </div>
                        <div>
                            Date of Birth : {post.birthdayDateTime}
                        </div>
                        <br></br>
                        <br></br>
                    </div> 
                    
                    ):
                    null
                }
            </div>
        );
    }
}

export default PostList;