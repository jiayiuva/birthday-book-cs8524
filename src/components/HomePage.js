import React, { Component } from 'react';
import logo from "../uvalogo.png"
import { Link } from 'react-router-dom'
// import Toolbar from "@material-ui/core/Toolbar";
// import Button from "@material-ui/core/Button";
// import Link from '@material-ui/core/Link';

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };

        //this.handleChange = this.handleChange.bind(this)
        //this.handleSubmit = this.handleSubmit.bind(this)
    }



    render(){


        return(
        <div>
        	<img src={logo} width={300} height={120} mode='fit' alt="Logo" />
            <div>
                <h1>Welcome to our birthday book!</h1>
            </div>
            <div>
                {/*<Link to = "/login" style={{ textDecoration: 'none', color: 'blue'}}> Login </Link>*/}
                <p style={{fontSize: '18px'}}>Already have an account?</p>
                <Link to = "/login" style={{ textDecoration: 'none', color: 'blue', fontSize: '18px'}}> Login Here </Link>
                <br></br>
                <br></br>
                <p style={{fontSize: '18px'}}>Do not have an account?</p>
                <Link to = "/register" style={{ textDecoration: 'none', color: 'blue', fontSize: '18px'}}> Register Here </Link>
            </div>

        </div>
        );

    }
}

export default HomePage;
