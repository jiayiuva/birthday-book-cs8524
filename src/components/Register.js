import React, { Component } from 'react';
import axios from 'axios'
import { Link } from 'react-router-dom'
import {Conf} from './Conf';
import logo from "../uvalogo.png"
import Button from '@material-ui/core/Button';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
           firstname : "",
           lastname : "",
           username : "",
           password : ""
        }
        this.state2 = {
            fullName : "",
            username : "",
            password : ""
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event){
        
        this.setState({
            [event.target.name] : event.target.value
        })//target这里指的是下面input模块，target.name即 input模块的"name"，setstate也就是把name和一个state对上了
    }

    
    handleSubmit(event){
        event.preventDefault()
        console.log(this.state)
        this.state2.fullName = this.state.firstname + ' ' + this.state.lastname
        this.state2.username = this.state.username
        this.state2.password = this.state.password
        axios

			.post('http://'+Conf+':8080/birthdaybookuser/register', this.state2)
			.then(response => {
                alert("successfully registered!")
				console.log(response)
			})
			.catch(error => {
				console.log(error)
			})
    }
   
    
    render() {
        return (
            <div>
                <img src={logo} width={300} height={120} mode='fit' alt="Logo" />
                <div>
                    <h1>Please Register here ! </h1>
                    <br></br>
                </div>
                <form onSubmit = {this.handleSubmit}>
                    <div>
                        <label> First Name </label>
                        <input type = 'text' 
                            name = "firstname"
                            value = {this.state.fullName} 
                            onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <label> Last Name </label>
                        <input type = 'text'
                               name = "lastname"
                               value = {this.state.lastname}
                               onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <label> Username </label>
                        <input type = 'text' 
                            name = "username"
                            value = {this.state.username} 
                            onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>
                    <div>
                        <label> Password </label>
                        <input type = 'password' 
                            name = "password"
                            value = {this.state.password} 
                            onChange = {this.handleChange}>
                        </input>
                    </div>
                    <br></br>

                    <div>
                        {/* <button type = 'submit'>Register</button> */}
                        <Button type = 'submit' variant="contained" color="primary">
                            Register
                        </Button>
                    </div>
                    <br></br>
                    <div>
                    <Link to = "/login" >Here to login!</Link>
                    </div>
                    
                </form>
            </div>
        );
            
    }
}

export default Register;